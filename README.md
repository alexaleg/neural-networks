# Neural Networks from Scratch

This project is a spinoff of week - 2's honours assignment from [HSE's - Introduction to deep learning](https://github.com/hse-aml/intro-to-dl).
The goal of this assignment is to implement a neural network from scratch.

## Goals
In this repo, I refactor the boiler plate code provided. 
My aim is to have a tested repo with the following features:
1. A Layer API that allows to construct neural networks with:
    1. Dense layers
    2. Different activation functions: `sigmoid`, `tanh`, `ReLU`, `LeakyReLU`
    3. Different optimization functions: `gradient_descent`, `gradient_descent_with_momentum`, among others.
    4. Dropout layers.
2. A network interface that:
    1. Displays layers in readable form for debugging purposes.
    2. Predicts results and trains the network.
    3. Displays training process.


