import layers as L
import numpy as np
from utils import eval_numerical_gradient

class TestDense_SGD:
    def test_forward(self):
        l = L.Dense(3,4)
        x = np.linspace(-1,1,2*3).reshape([2,3])
        l.weights = np.linspace(-1,1,3*4).reshape([3,4])
        l.biases = np.linspace(-1,1,4)

        assert np.allclose(l.forward(x),np.array([[ 0.07272727,  0.41212121,  0.75151515,  1.09090909],
                                              [-0.90909091,  0.08484848,  1.07878788,  2.07272727]]))

    def test_gradient(self):
        x = np.linspace(-1,1,10*32).reshape([10,32])
        l = L.Dense(32,64,learning_rate=0)

        numeric_grads = eval_numerical_gradient(lambda x: l.forward(x).sum(),x)
        grads = l.backward(x,np.ones([10,64]))

        assert np.allclose(grads,numeric_grads,rtol=1e-3,atol=0), "input gradient does not match numeric grad"

    def test_gradient_p(self):
        def compute_out_given_wb(w,b):
            l = L.Dense(32,64,learning_rate=1)
            l.weights = np.array(w)
            l.biases = np.array(b)
            x = np.linspace(-1,1,10*32).reshape([10,32])
            return l.forward(x)

        def compute_grad_by_params(w,b):
            l = L.Dense(32,64,learning_rate=1)
            l.weights = np.array(w)
            l.biases = np.array(b)
            x = np.linspace(-1,1,10*32).reshape([10,32])
            l.backward(x,np.ones([10,64]))
            return w - l.weights, b - l.biases

        w,b = np.random.randn(32,64), np.linspace(-1,1,64)

        numeric_dw = eval_numerical_gradient(lambda w: compute_out_given_wb(w,b).mean(0).sum(),w )
        numeric_db = eval_numerical_gradient(lambda b: compute_out_given_wb(w,b).mean(0).sum(),b )
        grad_w,grad_b = compute_grad_by_params(w,b)

        assert np.allclose(numeric_dw,grad_w,rtol=1e-3,atol=0), "weight gradient does not match numeric weight gradient"
        assert np.allclose(numeric_db,grad_b,rtol=1e-3,atol=0), "weight gradient does not match numeric weight gradient"

class TestActivations():
    def test_ReLU_shape(self):
        l = L.Activation(actfun="ReLU")
        x = np.linspace(-2,1,2*3).reshape([2,3])
        assert l.forward(x).shape == x.shape

    def test_ReLU_forward(self):
        l = L.Activation(actfun="ReLU")
        x = np.linspace(-2,1,2*3).reshape([2,3])
        assert np.allclose(l.forward(x), np.array([[0,0,0], [0, 0.4, 1]]), atol=1e-04)

    def test_ReLU_backwards(self):
        l = L.Activation(actfun="ReLU")
        x = np.linspace(-2,1,2*3).reshape([2,3])
        grad_output = np.linspace(-1,1,2*3).reshape([2,3])
        assert np.allclose(l.backward(x, grad_output), np.array([[0,0,0], [0, 0.6, 1]]), atol=1e-04)
