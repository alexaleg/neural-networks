import solvers
import numpy as np

def test_sgd_1():
    assert solvers.SGD(1).step(1,1)==0

def test_sgd_2():
    assert solvers.SGD(0.5).step(1,1)==0.5

w = np.array([1])
def test_sgd_m_1():
    assert solvers.SGD_M(1).step(w,1) == 0

def test_sgd_m_2():
    nu = w*0.1
    assert np.abs(solvers.SGD_M(1,1,nu).step(w,1) +0.1*w) <= 1e-5

def test_sgd_m_3():
    exp = 0.1
    sol = solvers.SGD_M(alpha=1)
    w1 = sol.step(w,1)
    assert np.abs(sol.step(w1,1) - np.array([exp]))<= 1e-5
