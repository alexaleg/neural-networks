import layers as L

class Network:
    """This class defines a neural network
    Attributes:
    - layers: array of layers of the network"""
    def __init__(self, layers=[]):
        self.layers = layers

    def add_layer(self,layer):
        assert isinstance(layer, L.Layer), 'input is not valid layer'
        self.layers.append(layer)

    def forward(self, X):
        """
        Compute activations of all network layers by applying them sequentially.
        Return a list of activations for each layer.
        Make sure last activation corresponds to network logits.
        """
        assert len(self.layers) > 0,'Network does not have any layer'
        activations = []
        input = X

        for layer in self.layers:
            input = layer.forward(input)
            activations.append(input)

        assert len(activations) == len(network)
        return activations


    def predict(self,X):
        """
        Compute network predictions.
        """
        assert len(self.layers) > 0,'Network does not have any layer'
        logits = self.forward(X)[-1]
        return logits.argmax(axis=-1)

    def step(self,X,y):
        """
        Computes an optimizer step for network on a given batch of X and y.
        """
        assert len(self.layers) > 0,'Network does not have any layer'
        from src.utils import grad_softmax_crossentropy_with_logits,softmax_crossentropy_with_logits
        layer_activations = self.forward(self,X)
        layer_inputs = [X]+layer_activations  #layer_input[i] is an input for network[i]
        logits = layer_activations[-1]
        # Compute the loss and the initial gradient
        loss = softmax_crossentropy_with_logits(logits,y)
        loss_grad = grad_softmax_crossentropy_with_logits(logits,y)
        grad_output = loss_grad
        # Backpropagation of gradients
        for i in range(len(self.layers)-1,-1,-1):
            grad_output = self.layers[i].backward(layer_inputs[i],grad_output)
        return np.mean(loss)

    def __str__(self):
        if len(self.layers)==0:
            return 'Neural network with no layers'
        layers = [ layer.__str__() for layer in self.layers]
        return '\n'.join(layers)


# def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
    # assert len(inputs) == len(targets)
    # if shuffle:
        # indices = np.random.permutation(len(inputs))
    # for start_idx in tqdm_utils.tqdm_notebook_failsafe(range(0, len(inputs) - batchsize + 1, batchsize)):
        # if shuffle:
            # excerpt = indices[start_idx:start_idx + batchsize]
        # else:
            # excerpt = slice(start_idx, start_idx + batchsize)
        # yield inputs[excerpt], targets[excerpt]

# for epoch in range(25):

    # for x_batch,y_batch in iterate_minibatches(X_train,y_train,batchsize=32,shuffle=True):
        # train(network,x_batch,y_batch)
