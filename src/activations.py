import numpy as np

class ReLU():
    def __init__(self):
        pass
    def forward(self,input_):
        return np.fmax(input_,0)
    def backward(self,input_, grad_output):
        assert input_.shape == grad_output.shape, "Input and gradient shapes mismatch"
        return np.where(input_>0,1,0)*grad_output

act_dict = {'ReLU': ReLU() }
def parse_activation(act):
    return act_dict[act]


