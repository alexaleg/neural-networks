import numpy as np


class Solver:
    """Abstract class to set up solvers"""
    def __init__(self):
        pass
    def step(self):
        pass

class SGD(Solver):
    """This class implements stochastic gradient descent
    Attributes:
    - eta: learning_rate"""
    def __init__(self, eta):
        self.eta = eta
    def step(self, weights, grad):
        return weights - self.eta*grad

class SGD_M(SGD):
    """Implementation of Stochastic Gradient Descent with momentum
    Attributes:
    - eta: learning_rate
    - alpha: parameter that multiplies nu
    - nu: momentum
    Equations:
        nu = alpha*nu + eta*gradient
        weight = weight - nu
    """
    def __init__(self, eta = 0.3, alpha = 0.9,nu = None):
        self.eta = eta
        self.alpha = alpha
        self.nu = nu
    def step(self, weights, grad):
        if not self.nu:
            self.nu = np.zeros_like(weights)
        print(self.nu.shape==weights.shape)
        assert self.nu.shape == weights.shape,'Weights and momentum should have the same dimensions'
        self.nu = self.alpha*self.nu + self.eta*grad
        print(self.nu)
        return weights - self.nu

def parse(solver):
    parse = {'SGD': SGD, 'SGD_M':SGD_M}
    return parse[solver]
