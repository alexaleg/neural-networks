import numpy as np
import solvers as solvers
import activations as act

class Layer:
    """
    Attributes:
    - type: it defines the type of a layer for printing purposes.
    A building block. Each layer is capable of performing two things:
    - Process input to get output:           output = layer.forward(input)
    - Propagate gradients through itself:    grad_input = layer.backward(input, grad_output)
    Some layers also have learnable parameters which they update during layer.backward.
    """
    def __init__(self):
        """Here you can initialize layer parameters (if any) and auxiliary stuff."""
        self.type = 'Layer'

    def forward(self, input_):
        """
        Takes input data of shape [batch, input_units], returns output data [batch, output_units]
        """
        # A dummy layer just returns whatever it gets as input.
        return input_

    def backward(self, input_, grad_output):
        """
        Performs a backpropagation step through the layer, with respect to the given input.
        """
        num_units = input_.shape[1]
        d_layer_d_input_ = np.eye(num_units)
        return np.dot(grad_output, d_layer_d_input) # chain rule
    def __str__(self):
        return f'Layer of type {self.type}'

class Activation(Layer):
    def __init__(self, actfun = 'ReLU', **kwargs):
        """Activation layer simply applies elementwise the activation function set by actfun to all inputs"""
        self.type = 'Activation'
        self.actfun = act.parse_activation(actfun)
    def forward(self, input_):
        """Apply elementwise activation to [batch, input_units] matrix"""
        return self.actfun.forward(input_)
    def backward(self, input_, grad_output):
        """Compute gradient of loss w.r.t. actfun input"""
        return self.actfun.backward(input_,grad_output)

class Dense(Layer):
    def __init__(self, input_units, output_units, solver='SGD', learning_rate=0.1, **kwargs):
        """
        A dense layer is a layer which performs a learned affine transformation:
        f(x) = <W*x> + b
        """
        self.type = 'Dense'
        self.learning_rate = learning_rate
        # initialize weights with small random numbers. We use normal initialization, 
        self.weights = np.random.randn(input_units, output_units)*0.01
        self.biases = np.zeros(output_units)
        self.solver = solvers.parse(solver)(learning_rate)
    def forward(self,input_):
        """
        Perform an affine transformation:
        f(x) = <W*x> + b
        input shape: [batch, input_units]
        output shape: [batch, output units]
        """
        return np.dot(input_,self.weights) + self.biases

    def backward(self,input_,grad_output):
        # compute d f / d x = d f / d dense * d dense / d x
        # where d dense/ d x = weights transposed
        grad_input = np.matmul(grad_output,self.weights.T)
        # compute gradient w.r.t. weights and biases
        grad_weights = np.dot(input_.T,grad_output)/input_.shape[0]
        grad_biases = grad_output.mean(axis=0)
        assert grad_weights.shape == self.weights.shape and grad_biases.shape == self.biases.shape
        # Stack weights and biases
        joined = np.vstack([self.weights, self.biases])
        joined_grad = np.vstack([grad_weights,grad_biases])

        solved = self.solver.step(joined, joined_grad)

        self.weights = solved[:-1,:]
        self.biases = solved[-1,:]
        return grad_input

    def __str__(self):
        return f'{self.type} layer of shape {self.weights.shape}'
